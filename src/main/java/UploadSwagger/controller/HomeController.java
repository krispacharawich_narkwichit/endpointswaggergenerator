package UploadSwagger.controller;

import UploadSwagger.service.UploadService;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.core.io.ByteArrayResource;
import org.springframework.core.io.InputStreamResource;
import org.springframework.core.io.Resource;
import org.springframework.http.HttpHeaders;
import org.springframework.http.HttpStatus;
import org.springframework.http.MediaType;
import org.springframework.http.ResponseEntity;
import org.springframework.web.bind.annotation.PostMapping;
import org.springframework.web.bind.annotation.RequestParam;
import org.springframework.web.bind.annotation.RestController;
import org.springframework.web.multipart.MultipartFile;
import org.springframework.web.servlet.mvc.support.RedirectAttributes;

import java.io.IOException;
import java.util.Arrays;

@RestController
public class HomeController {
        private static final Logger logger = LoggerFactory.getLogger(HomeController.class);

        @Autowired
        private UploadService uploadService;

        @PostMapping("/upload")
        public ResponseEntity<Resource> handleFileUpload(@RequestParam("file")MultipartFile multipartFile) throws IOException{

            logger.info("Single file upload!");
            uploadService.purgeAllFiles();
            if (multipartFile.isEmpty()) {
                return new ResponseEntity("please select a file !", HttpStatus.OK);
            }
            if (!multipartFile.getOriginalFilename().endsWith("json"))
            {
                return new ResponseEntity("Accept only swagger json file !", HttpStatus.OK);
            }

            try{
                uploadService.saveUploadedFiles(Arrays.asList(multipartFile));

            }catch (IOException e) {
                return new ResponseEntity<>(HttpStatus.BAD_REQUEST);
            }
            uploadService.transformSwaggerFile();
            ByteArrayResource byteArrayResource = uploadService.zipAllSwaggerFiles();
            HttpHeaders headers = this.getHttpHeaders();
            headers.add("Content-Disposition", String.format("inline; filename=\"" + "SwaggerFiles.zip" + "\""));
            uploadService.purgeAllFiles();
            return ResponseEntity.ok()
                    .headers(headers)
                    .contentLength(byteArrayResource.contentLength())
                    .contentType(MediaType.parseMediaType("application/octet-stream"))
                    .body(byteArrayResource);
        }

        private HttpHeaders getHttpHeaders() {
            HttpHeaders headers = new HttpHeaders();
            headers.add("Cache-Control", "no-cache, no-store, must-revalidate");
            headers.add("Pragma", "no-cache");
            headers.add("Expires", "0");
            return headers;
        }


}
