package UploadSwagger.service;

import UploadSwagger.constant.CommonConstant;
import UploadSwagger.entity.MySwagger;
import UploadSwagger.utils.FileUtils;
import io.swagger.models.Operation;
import io.swagger.models.Path;
import io.swagger.models.Response;
import io.swagger.models.Swagger;
import io.swagger.models.properties.Property;
import io.swagger.models.properties.RefProperty;
import io.swagger.models.properties.StringProperty;
import io.swagger.util.Json;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;
import org.springframework.util.StringUtils;

import java.io.File;
import java.io.IOException;
import java.util.Iterator;
import java.util.List;
import java.util.Map;

@Service("SwaggerService")
public class SwaggerServiceImpl implements SwaggerService {
    private static final Logger logger = LoggerFactory.getLogger(SwaggerServiceImpl.class);

    @Autowired
    SwaggerOperationService swaggerOperationService;

    private List<File> swaggerFileList;
    private String currentTag;

    public void swaggerScan(String fileFilter) throws IOException {
        logger.info(">>> START : SwaggerServiceImpl.swaggerScan >>>");
        swaggerFileList = StringUtils.isEmpty(fileFilter) ? FileUtils.listAllFile(CommonConstant.UPLOAD_FOLDER)
                : FileUtils.listAllFileByFilter(CommonConstant.UPLOAD_FOLDER, fileFilter);
        logger.info("<<< END : SwaggerServiceImpl.swaggerScan <<<");
    }

    public void readAllSwaggerFiles() {
        logger.info(">>> START : SwaggerServiceImpl.readAllSwaggerFiles >>>");
        swaggerFileList.stream().forEach(file -> {
            String jsonFile = CommonConstant.UPLOAD_FOLDER.concat(file.getName());
            System.out.println("JSONFILE: "+jsonFile);
            this.beautifySwaggerFile(jsonFile);
        });
        logger.info("<<< END : SwaggerServiceImpl.readAllSwaggerFiles <<<");
    }

    private void beautifySwaggerFile(String jsonFile) {
        logger.info(">>> START : SwaggerServiceImpl.beautifySwaggerFile >>>");
        List<MySwagger> listSwagger = swaggerOperationService.buildListSwaggerEndpoint(jsonFile);

        for (int i = 0; i < listSwagger.size(); i++) {
            MySwagger mySwagger = new MySwagger();
            System.out.println("METHOD: "+listSwagger.get(i).getMethod());
            mySwagger.setMethod(listSwagger.get(i).getMethod());
            mySwagger.setSwagger(listSwagger.get(i).getSwagger());
            mySwagger.setOperation(listSwagger.get(i).getOperation());
            mySwagger.setPathKey(listSwagger.get(i).getPathKey());
            mySwagger.setTag(listSwagger.get(i).getTag());
            Swagger newSwagger = mySwagger.getSwagger();
            newSwagger.setDefinitions(swaggerOperationService.rewriteDefinition(mySwagger));
            Operation operation = mySwagger.getOperation();
            String pathKey = mySwagger.getPathKey();
            if (mySwagger.getTag() != null)
                this.currentTag = mySwagger.getTag().getName();

            Response response = operation.getResponses().get(CommonConstant.HTTP_CODE_200);
            if (response != null) {
                Property property = response.getSchema();

                if (response.getSchema() instanceof RefProperty) {
                    RefProperty refProp = (RefProperty) response.getSchema();
                } else if (response.getSchema() instanceof StringProperty) {
                }
            }
            StringBuilder bd = new StringBuilder();
            String rootPathWithTag = null;
            String rootPath = CommonConstant.UPLOAD_FOLDER;
            if (mySwagger.getTag() != null) {
                rootPathWithTag = rootPath + mySwagger.getTag().getName();
            } else {
                rootPathWithTag = rootPath;
            }
            File tagDirectory = new File(rootPathWithTag);
            if (!tagDirectory.exists()) {
                tagDirectory.mkdir();
            }
            bd.append(rootPathWithTag + "/");
            bd.append(mySwagger.getMethod());
            bd.append(pathKey.replaceAll("\\/", "_"));

            try {
                this.writeEachApiToJsonFile(mySwagger, bd);
            } catch (Exception e) {
                e.printStackTrace();
            }
        }
        logger.info("<<< END : SwaggerServiceImpl.beautifySwaggerFile <<<");
    }

    private void writeEachApiToJsonFile(MySwagger mySwagger, StringBuilder bd) throws Exception {
        logger.info(">>> START : SwaggerServiceImpl.writeEachApiToJsonFile >>>");
        Swagger swagger = mySwagger.getSwagger();
        swagger.setDefinitions(swaggerOperationService.rewriteDefinition(mySwagger));
        Iterator<Map.Entry<String,Path>> iter = swagger.getPaths().entrySet().iterator();
        while(iter.hasNext())
        {
            Map.Entry<String, Path> pair = iter.next();
            Path p = pair.getValue();
            p.setParameters(null);
        }
        FileUtils.writeContentToHTML(
                Json.pretty().writeValueAsString(swagger),
                bd.toString().concat(CommonConstant.JSON_FILE_EXT));
        logger.info("<<< END : SwaggerServiceImpl.writeEachApiToJsonFile <<<");
    }

}
