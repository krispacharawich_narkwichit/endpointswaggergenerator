package UploadSwagger.service;

import UploadSwagger.entity.MySwagger;
import io.swagger.models.Model;
import io.swagger.models.Swagger;

import java.util.List;
import java.util.Map;

public interface SwaggerOperationService {
    public List<MySwagger> buildListSwaggerEndpoint(String jsonFile);

    public Swagger readSwaggerFile(String jsonFile);

    public Map<String, Model> rewriteDefinition(MySwagger mySwagger);

    public List<MySwagger> groupListSwaggersByTags(List<MySwagger> listMySwagger);

    public List<String> getTagsFromSwaggerFile(String swaggerJson);
}
