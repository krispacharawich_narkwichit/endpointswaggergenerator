package UploadSwagger.service;

import java.io.IOException;

public interface SwaggerService {
    public void swaggerScan(String fileFilter) throws IOException;
    public void readAllSwaggerFiles();
}
