package UploadSwagger.service;

import UploadSwagger.constant.CommonConstant;
import UploadSwagger.entity.MySwagger;
import com.fasterxml.jackson.databind.ObjectMapper;
import io.swagger.models.*;
import io.swagger.models.parameters.BodyParameter;
import io.swagger.models.parameters.HeaderParameter;
import io.swagger.models.parameters.Parameter;
import io.swagger.models.properties.ArrayProperty;
import io.swagger.models.properties.Property;
import io.swagger.models.properties.RefProperty;
import io.swagger.parser.SwaggerParser;
import org.springframework.stereotype.Service;
import UploadSwagger.utils.FileUtils;
import java.util.*;

@Service("SwaggerOperationService")
public class SwaggerOperationServiceImpl implements SwaggerOperationService {

    @Override
    public List<MySwagger> buildListSwaggerEndpoint(String jsonFile) {
        System.out.println(">>> SwaggerOperationServiceImpl.buildListSwaggerEndpoint >>");

        // Some files contain unreadable unicode characters: 65533-REPLACEMENT CHARACTER, \0: NUL
        FileUtils fileUtils = new FileUtils();
        FileUtils.rewriteFileByRemoveUnreadableUnicodeChar(jsonFile);

        Swagger swagger = readSwaggerFile(jsonFile);

        List<MySwagger> listSwagger = new ArrayList<>();
        Iterator<Map.Entry<String, Path>> iterPath = swagger.getPaths().entrySet().iterator();
        List<Tag> iterTag = swagger.getTags();

        while (iterPath.hasNext()) {
            Map.Entry<String, Path> objPath = iterPath.next();
            String pathKey = objPath.getKey();
            Path pathOperation = objPath.getValue();
            Operation operation = null;
            for (int i = 0; i < pathOperation.getOperations().size(); i++) {
                MySwagger mySwagger = new MySwagger();
                Operation operItem = pathOperation.getOperations().get(i);
                Path thisPath = new Path();
                if ((pathOperation.getPost() != null) && (pathOperation.getPost() == operItem)) {
                    operation = pathOperation.getPost();
                    thisPath.setPost(operItem);
                    mySwagger.setMethod(CommonConstant.POST_METHOD);
                } else if ((pathOperation.getGet() != null) && (pathOperation.getGet() == operItem)) {
                    operation = pathOperation.getGet();
                    thisPath.setGet(operItem);
                    mySwagger.setMethod(CommonConstant.GET_METHOD);

                } else if ((pathOperation.getPut() != null) && (pathOperation.getPut() == operItem)) {
                    operation = pathOperation.getPut();
                    thisPath.setPut(operItem);
                    mySwagger.setMethod(CommonConstant.PUT_METHOD);

                } else if ((pathOperation.getDelete() != null) && (pathOperation.getDelete() == operItem)) {
                    operation = pathOperation.getDelete();
                    thisPath.setDelete(operItem);
                    mySwagger.setMethod(CommonConstant.DELETE_METHOD);
                }
                thisPath.setParameters(operItem.getParameters());
                thisPath.setVendorExtensions(operItem.getVendorExtensions());
                List<Tag> listTag = new ArrayList<>();
                if (operation.getTags() != null) {
                    for (String tag : operation.getTags()) {
                        Tag t = new Tag();
                        t.setName(tag);
                        mySwagger.setTag(t);
                        listTag.add(t);
                    }
                }else {
                    if (iterTag != null) {
                        for (Tag t : iterTag) {
                            listTag.add(t);
                            mySwagger.setTag(t);
                        }
                    }
                }

                HashMap<String, Path> hashMap = new HashMap();
                hashMap.put(pathKey, thisPath);
                Swagger newSwagger = this.createSwaggerEndpoint(swagger, operation, listTag, hashMap, pathKey);
                mySwagger.setSwagger(newSwagger);
                mySwagger.setOperation(operation);
                mySwagger.setPathKey(pathKey);
                listSwagger.add(mySwagger);
            }
        }
        return listSwagger;
    }

    @Override
    public List<MySwagger> groupListSwaggersByTags(List<MySwagger> listMySwagger) {
        Map<String, List<MySwagger>> listUniqueTags = new HashMap<>();

        for (int i = 0; i < listMySwagger.size(); i++) {
            Tag tag = listMySwagger.get(i).getTag();
            if (listUniqueTags.get(tag.getName()) == null) {
                List<MySwagger> tmpListSwagger = new ArrayList<>();
                tmpListSwagger.add(listMySwagger.get(i));
                listUniqueTags.put(tag.getName(), tmpListSwagger);
            } else {
                listUniqueTags.get(tag.getName()).add(listMySwagger.get(i));
            }
        }
        List<MySwagger> listFinalSwaggers = this.mergeDefFromListUniqTags(listUniqueTags);

        return listFinalSwaggers;
    }

    private List<MySwagger> mergeDefFromListUniqTags(Map<String, List<MySwagger>> listUniqueTags) {
        Iterator<Map.Entry<String, List<MySwagger>>> iterListUniqueTags = listUniqueTags.entrySet().iterator();
        List<MySwagger> listFinalMySwagger = new ArrayList<>();
        while (iterListUniqueTags.hasNext()) {
            Map.Entry<String, List<MySwagger>> pairListUniq = iterListUniqueTags.next();
            String tagName = pairListUniq.getKey();
            List<MySwagger> valueList = pairListUniq.getValue();
            MySwagger tmpMySwagger = valueList.get(0);
            Map<String, Model> mapDef = reWriteDefinitionDict(valueList);
            tmpMySwagger.getSwagger().setDefinitions(mapDef);
            listFinalMySwagger.add(tmpMySwagger);
        }
        return listFinalMySwagger;
    }

    private Swagger createSwaggerEndpoint(Swagger swagger, Operation operation, List<Tag> listTag,
                                          HashMap<String, Path> hashMap, String pathKey) {
        Swagger sw = new Swagger();
        sw.setBasePath(swagger.getBasePath());
        sw.setConsumes(operation.getConsumes());
        sw.setDefinitions(swagger.getDefinitions());
        sw.setExternalDocs(operation.getExternalDocs());
        sw.setHost(swagger.getHost());
        sw.setInfo(swagger.getInfo());
        sw.setParameters(swagger.getParameters());
        sw.setPaths(hashMap);
        sw.setProduces(operation.getProduces());
        sw.setResponses(operation.getResponses());
        sw.setSchemes(operation.getSchemes());
        sw.setSecurity(swagger.getSecurity());
        sw.setSecurityDefinitions(swagger.getSecurityDefinitions());
        sw.setSecurityRequirement(swagger.getSecurityRequirement());
        sw.setSwagger("2.0");
        sw.setTags(listTag);
        if (swagger.getVendorExtensions() != null) {
            Iterator<Map.Entry<String, Object>> vendor = swagger.getVendorExtensions().entrySet().iterator();
            while (vendor.hasNext()) {
                Map.Entry<String, Object> pair = vendor.next();
                sw.setVendorExtension(pair.getKey(), pair.getValue());
            }
        }
        return sw;
    }

    @Override
    public Swagger readSwaggerFile(String jsonFile) {
        Swagger swagger = new SwaggerParser().read(jsonFile);
        ObjectMapper objectMapper = new ObjectMapper();
        try {
            System.out.println("Show Swagger2: " + objectMapper.writeValueAsString(swagger));
        } catch (Exception e) {
            e.printStackTrace();
        }
        if (swagger == null)
            System.out.println("This file is Wrong: " + jsonFile);
        Map<String, Path> paths = swagger.getPaths();
        for (Map.Entry<String, Path> entry : paths.entrySet()) {
            List<Operation> operations = entry.getValue().getOperations();
            for (Operation operation : operations) {
                List<Parameter> newParameter = new ArrayList<>();
                HeaderParameter apiKey = new HeaderParameter();
                apiKey.setName("apiKey");
                apiKey.setIn("header");
                apiKey.setRequired(true);
                apiKey.setType("string");
                apiKey.setDescription("The apiKey used to authenticate access");
                newParameter.add(apiKey);
                HeaderParameter apiSecret = new HeaderParameter();
                apiSecret.setName("apiSecret");
                apiSecret.setIn("header");
                apiSecret.setRequired(true);
                apiSecret.setType("string");
                apiSecret.setDescription("The apiSecret used to authenticate access");
                newParameter.add(apiSecret);
                for (Parameter parameter : operation.getParameters()) {

                    if ("apikey".equalsIgnoreCase(parameter.getName()))
                    {
                        newParameter.remove(apiKey);
                    }
                    if ("apisecret".equalsIgnoreCase(parameter.getName()))
                    {
                        newParameter.remove(apiSecret);
                    }
                    if (!"sourceSystem".equalsIgnoreCase(parameter.getName())) {
                        newParameter.add(parameter);
                    }

                }

                operation.setParameters(newParameter);
            }

        }
        return swagger;
    }


    public Map<String, Model> reWriteDefinitionDict(List<MySwagger> mySwaggers) {
        List<MySwagger> listMySwagger = mySwaggers;
        Map<String, Model> maplistDef = new HashMap<>();
        for (int i = 0; i < listMySwagger.size(); i++) {
            MySwagger mySwag = listMySwagger.get(i);
            Map<String, Model> mapDef = rewriteDefinition(mySwag);
            Iterator<Map.Entry<String, Model>> iterDef = mapDef.entrySet().iterator();
            while (iterDef.hasNext()) {
                Map.Entry<String, Model> pair = iterDef.next();
                String key = pair.getKey();
                Model value = pair.getValue();
                maplistDef.put(key, value);
            }
        }
        return maplistDef;
    }

    public Map<String, Model> rewriteDefinition(MySwagger mySwagger) {
        Operation operation = mySwagger.getOperation();
        Swagger swagger = mySwagger.getSwagger();
        Map<String, Model> mapDef = new HashMap<>();

        // ReWrite RequestParameter Definition
        this.rewriteRequestParamDefinition(mapDef, operation, swagger);

        // ReWrite Response Definition
        Iterator<Map.Entry<String, Response>> iterResponse = operation.getResponses().entrySet().iterator();
        while (iterResponse.hasNext()) {
            Map.Entry<String, Response> pairResponse = iterResponse.next();
            Response responseValue = pairResponse.getValue();
            mapDef = this.mapResponseDefinition(mapDef, responseValue, swagger);
        }
        return mapDef;
    }

    private Map<String, Model> rewriteRequestParamDefinition(Map<String, Model> mapDef, Operation operation, Swagger swagger) {
        Iterator<Parameter> iterParameter = operation.getParameters().iterator();
        while (iterParameter.hasNext()) {
            Parameter parameter = iterParameter.next();
            if (parameter.getIn().equalsIgnoreCase("body")) {
                BodyParameter bodyParameter = (BodyParameter) parameter;
                if (bodyParameter.getSchema().getReference() != null) {
                    String requestReference = bodyParameter.getSchema().getReference();
                    String extractRequestRefName = requestReference.substring(
                            requestReference.lastIndexOf("/") + 1,
                            requestReference.length()
                    );
                    this.findDefinition(mapDef,
                            swagger,
                            extractRequestRefName);
                }

                // Link Request Parameters (Type - Array) to Reference Model
                if (bodyParameter.getSchema() instanceof ArrayModel) {
                    ArrayModel arrayModel = (ArrayModel) bodyParameter.getSchema();
                    if (arrayModel.getItems() instanceof RefProperty) {
                        RefProperty refProperty = (RefProperty) arrayModel.getItems();
                        String requestReference = refProperty.get$ref();
                        String extractRequestRefName = requestReference.substring(
                                requestReference.lastIndexOf("/") + 1,
                                requestReference.length()
                        );
                        this.findDefinition(mapDef,
                                swagger,
                                extractRequestRefName);
                    }
                }
            }
        }
        return mapDef;
    }

    private Map<String, Model> mapResponseDefinition(Map<String, Model> mapDef, Response response, Swagger swagger) {
        if (response.getSchema() instanceof RefProperty) {
            RefProperty refProp = (RefProperty) response.getSchema();
            this.readingRefProperty(mapDef, refProp, swagger);

        } else if (response.getSchema() instanceof ArrayProperty) {
            ArrayProperty arrayProperty = (ArrayProperty) response.getSchema();
            if (arrayProperty.getItems() instanceof RefProperty) {
                RefProperty refChildProp1 = (RefProperty) arrayProperty.getItems();
                this.readingRefProperty(mapDef, refChildProp1, swagger);
            }
        }
        return mapDef;
    }

    private void readingRefProperty(Map<String, Model> mapDef, RefProperty refProperty, Swagger swagger) {
        if ((refProperty != null) && (refProperty.get$ref() != null)) {
            RefProperty refChildProp = refProperty;
            String childRef = refChildProp.get$ref();
            String extractChildRefName = childRef.substring(childRef.lastIndexOf("/") + 1,
                    childRef.length());
            this.findDefinition(mapDef, swagger, extractChildRefName);
        }
    }

    private void findDefinition(Map<String, Model> mapDef, Swagger swagger, String extractChildRefName) {
        if (swagger.getDefinitions().get(extractChildRefName) != null) {
            Model childModel = swagger.getDefinitions().get(extractChildRefName);
            if (mapDef.get(extractChildRefName) == null) {
                mapDef.put(extractChildRefName, childModel);
                if (childModel.getProperties() != null) {
                    Iterator<Map.Entry<String, Property>> iterDefValueProp = childModel.getProperties().entrySet()
                            .iterator();
                    while (iterDefValueProp.hasNext()) {
                        Map.Entry<String, Property> pairDefValueProp = iterDefValueProp.next();
                        String defValueKey = pairDefValueProp.getKey();
                        if (mapDef.get(defValueKey) == null) {
                            if (pairDefValueProp.getValue() instanceof RefProperty) {
                                RefProperty refChildProp1 = (RefProperty) pairDefValueProp.getValue();
                                this.readingRefProperty(mapDef, refChildProp1, swagger);

                            } else if (pairDefValueProp.getValue() instanceof ArrayProperty) {
                                ArrayProperty arrayProperty = (ArrayProperty) pairDefValueProp.getValue();
                                if (arrayProperty.getItems() instanceof RefProperty) {
                                    RefProperty refChildProp1 = (RefProperty) arrayProperty.getItems();
                                    this.readingRefProperty(mapDef, refChildProp1, swagger);
                                }
                            }
                        }
                    }
                }
            }
        }
    }

    public List<String> getTagsFromSwaggerFile(String swaggerJson) {
        List<String> listTags = new ArrayList<>();
        HashSet<String> setTag = new HashSet<>();
        Swagger swagger = readSwaggerFile(swaggerJson);
        if (swagger.getPaths() != null) {
            Iterator<Map.Entry<String, Path>> iterPath = swagger.getPaths().entrySet().iterator();
            while (iterPath.hasNext()) {
                Map.Entry<String, Path> objPath = iterPath.next();
                Path pathOperation = objPath.getValue();
                Operation operation = null;

                if (pathOperation.getPost() != null) {
                    operation = pathOperation.getPost();
                } else if (pathOperation.getGet() != null) {
                    operation = pathOperation.getGet();
                } else if (pathOperation.getPut() != null) {
                    operation = pathOperation.getPut();
                } else if (pathOperation.getDelete() != null) {
                    operation = pathOperation.getDelete();
                }

                if (operation.getTags() != null) {
                    for (String tag : operation.getTags()) {
                        setTag.add(tag);
                    }
                }
            }
            if (setTag.isEmpty()) {
                listTags.add(CommonConstant.TAG_UNDEFINED);
            } else {
                Iterator<String> iterSetTag = setTag.iterator();
                while (iterSetTag.hasNext()) {
                    String tagName = iterSetTag.next();
                    listTags.add(tagName);
                }
            }
        } else if (swagger.getTags() != null) {
            for (Tag tag : swagger.getTags()) {
                listTags.add(tag.getName());
            }
        } else {
            listTags.add(CommonConstant.TAG_UNDEFINED);
        }
        return listTags;
    }
}
