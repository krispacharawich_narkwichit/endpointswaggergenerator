package UploadSwagger.service;

import UploadSwagger.constant.CommonConstant;
import UploadSwagger.utils.FileUtils;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.core.io.ByteArrayResource;
import org.springframework.stereotype.Service;
import org.springframework.web.multipart.MultipartFile;

import java.io.File;
import java.io.IOException;
import java.nio.file.Files;
import java.nio.file.Path;
import java.nio.file.Paths;
import java.util.*;

@Service("UploadService")
public class UploadService {

    private static final Logger logger = LoggerFactory.getLogger(UploadService.class);

    @Autowired
    SwaggerServiceImpl swaggerService;

    private List<String> listFiles = new ArrayList<>();

    public void saveUploadedFiles(List<MultipartFile> files) throws IOException {
        logger.info(">>> UploadService.saveUploadedFiles >>>");
        for (MultipartFile file : files) {
            if (!file.isEmpty()) {
                byte[] bytes = file.getBytes();
                String fullPath = CommonConstant.UPLOAD_FOLDER + file.getOriginalFilename();
                Path path = Paths.get(fullPath);
                logger.info("Save uploaded file at path: " + path.getParent() + ", " + path.getFileName());
                Files.write(path, bytes);
                listFiles.add(fullPath);
            }
        }
        logger.info(">>> UploadService.saveUploadedFiles >>>");
    }

    public void transformSwaggerFile() {
        logger.info(">>> UploadService.transformSwaggerFile >>>");
        listFiles.stream().forEach(file -> {
            try {
                swaggerService.swaggerScan(Paths.get(file).getFileName().toString());
            } catch (IOException e) {
                logger.error("ERROR");
                e.printStackTrace();
            }
        });
        swaggerService.readAllSwaggerFiles();
        logger.info("<<< UploadService.transformSwaggerFile <<<");
    }

    public ByteArrayResource zipAllSwaggerFiles() throws IOException {
        List<String> listTag = new ArrayList<>();
        File file = new File(CommonConstant.UPLOAD_FOLDER);
        File[] listFolders = FileUtils.listFolder(file);
        for (File f : listFolders) {
            String zipPath = CommonConstant.ZIP_FOLDER + f.getName() + CommonConstant.ZIP_FORMAT;
            File tagFolder = new File(CommonConstant.UPLOAD_FOLDER + "/" + f.getName() + "/");
            List<File> listSwaggerFiles = Arrays.asList(tagFolder.listFiles());
            try {
                FileUtils.zipFiles(listSwaggerFiles, zipPath);
                listTag.add(f.getName());
            } catch (Exception e) {
                e.printStackTrace();
            }
        }

        if (listTag.size() > 1) {
            this.nestZip();
            String pathStr = CommonConstant.ZIP_FOLDER + CommonConstant.ALL_TAGS_ZIP_NAME + CommonConstant.ZIP_FORMAT;
            System.out.println("Show PATH: " + pathStr);
            ByteArrayResource byteArrayResource = new ByteArrayResource(Files.readAllBytes(Paths.get(pathStr)));
            return byteArrayResource;
        } else {
            String pathStr = CommonConstant.ZIP_FOLDER + listTag.get(0) + CommonConstant.ZIP_FORMAT;
            System.out.println("Show PATH: "+pathStr);
            ByteArrayResource byteArrayResource = new ByteArrayResource(Files.readAllBytes(Paths.get(
                    pathStr)));
            return byteArrayResource;
        }
    }

    private void nestZip() {
        String zipPath = CommonConstant.ZIP_FOLDER + CommonConstant.ALL_TAGS_ZIP_NAME + CommonConstant.ZIP_FORMAT;
        File zipFolder = new File(CommonConstant.ZIP_FOLDER);
        List<File> listSwaggerFiles = Arrays.asList(zipFolder.listFiles());
        try {
            FileUtils.zipFiles(listSwaggerFiles, zipPath);
        } catch (Exception e) {
            e.printStackTrace();
        }

    }

    public void purgeAllFiles() throws IOException {
        org.apache.commons.io.FileUtils.cleanDirectory(new File(CommonConstant.ZIP_FOLDER));
        File uploadFolder = new File(CommonConstant.UPLOAD_FOLDER);
        File[] listFile = uploadFolder.listFiles();
        for(File file : listFile)
        {
            if (file.isDirectory())
            {
                org.apache.commons.io.FileUtils.deleteDirectory(file);
            }else {
                file.delete();
            }
        }
        File rootFolder = new File(CommonConstant.ROOT_FOLDER);
        File[] listRootFile = rootFolder.listFiles();
        for(File file: listRootFile)
        {
            if (file.isFile())
            {
                file.delete();
            }
        }

    }


}
