package UploadSwagger.configuration;

import UploadSwagger.constant.CommonConstant;
import org.springframework.stereotype.Component;

import javax.annotation.PostConstruct;
import java.io.File;

@Component
public class DirectoryFactory {

    @PostConstruct
    public void initDirectory() {
        System.out.println("INITAIAL DIRECTORY");
        File rootFolder = new File(CommonConstant.ROOT_FOLDER);
        if (!rootFolder.exists())
             rootFolder.mkdir();

        File uploadFile = new File(CommonConstant.UPLOAD_FOLDER);
        if (! uploadFile.exists())
            uploadFile.mkdir();


        File zipfolder = new File(CommonConstant.ZIP_FOLDER);
        if (! zipfolder.exists())
             zipfolder.mkdir();


    }
}
