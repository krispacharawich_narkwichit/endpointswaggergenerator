package UploadSwagger.utils;

import UploadSwagger.service.SwaggerServiceImpl;
import org.apache.commons.io.output.FileWriterWithEncoding;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;
import org.springframework.util.StringUtils;

import java.io.*;
import java.nio.charset.Charset;
import java.nio.charset.StandardCharsets;
import java.util.ArrayList;
import java.util.List;
import java.util.zip.ZipEntry;
import java.util.zip.ZipOutputStream;

public class FileUtils {
    private static final Logger logger = LoggerFactory.getLogger(FileUtils.class);

    public static List<String> listAllFileName(String path) {
        return listFileName(new File(path), null, ".json");
    }

    public static List<String> listAllFileNameByFilter(String path, String filter) {
        return listFileName(new File(path), filter, ".json");
    }

    public static List<File> listAllFile(String path) {
        return listFiles(new File(path), null, ".json");
    }

    public static List<File> listAllFileByFilter(String path, String filter) {
        return listFiles(new File(path), filter, ".json");
    }

    /**
     * Get all file name in current path
     *
     * @param dir
     * @param filter
     * @param extension
     * @return file name
     */
    private static List<String> listFileName(final File dir, String filter, String extension) {
        List<String> files = new ArrayList<String>();
        for (final File file : dir.listFiles()) {
            if (file.isDirectory()) {
                listFileName(file, filter, extension);
            } else if (matchFilter(file.getName(), filter) && matchExtension(file.getName(), extension)) {
                files.add(file.getName());
            }
        }
        return files;
    }

    /**
     * Get all file in current path
     *
     * @param dir
     * @param filter
     * @param extension
     * @return file
     */
    private static List<File> listFiles(final File dir, String filter, String extension) {
        List<File> files = new ArrayList<File>();
        for (final File file : dir.listFiles()) {
            if (file.isDirectory()) {
                listFiles(dir, filter, extension);
            } else if (matchFilter(file.getName(), filter) && matchExtension(file.getName(), extension)) {
                files.add(file);
            }
        }
        return files;
    }

    private static boolean matchFilter(String fileName, String filter) {
        return StringUtils.isEmpty(filter) || fileName.contains(filter);
    }

    private static boolean matchExtension(String fileName, String extension) {
        return StringUtils.isEmpty(extension) ? fileName.endsWith(".json") : fileName.endsWith(extension);
    }

    public static void rewriteFileByRemoveUnreadableUnicodeChar(String jsonFile) {
		logger.info(">>> FileUtils.rewriteFileByRemoveUnreadableUnicodeChar() >>> param[jsonFile]: "+jsonFile);
        StringBuilder stringBuilder = new StringBuilder();
        String replacementCharacter = "\uFFFD";
        String unicodeNull = "\0";
        try {
            String readFile = org.apache.commons.io.FileUtils.readFileToString(new File(jsonFile));
			logger.info("ReadFile: "+readFile);
            stringBuilder = new StringBuilder(readFile);

            if (readFile.contains(replacementCharacter)) {
                stringBuilder = new StringBuilder(stringBuilder.toString().replaceAll(replacementCharacter, ""));
            }
            if (stringBuilder.toString().contains(unicodeNull)) {
                stringBuilder = new StringBuilder(stringBuilder.toString().replaceAll(unicodeNull, ""));
            }
            org.apache.commons.io.FileUtils.writeStringToFile(new File(jsonFile), stringBuilder.toString(), StandardCharsets.UTF_8);
        } catch (Exception e) {
            e.printStackTrace();
        }
    }

    public static void writeContentToHTML(String contents, String fileName) throws IOException {
        FileWriterWithEncoding fw = null;
        BufferedWriter bw = null;

        try {
            fw = new FileWriterWithEncoding(fileName, Charset.forName("UTF-8"));
            bw = new BufferedWriter(fw);
            bw.write(contents);

        } catch (IOException e) {
            e.printStackTrace();
            throw e;
        } finally {
            try {
                if (bw != null) bw.close();
                if (fw != null) fw.close();
            } catch (IOException ex) {
                ex.printStackTrace();
                throw ex;
            }
        }

    }

    public static boolean zipFiles(List<File> listFiles, String zipPath) throws Exception {
        logger.info(" >>> FileUtils.zipFiles >>> ");
        logger.info(" Zip Path: " + zipPath);
        FileOutputStream fos = new FileOutputStream(zipPath);
        ZipOutputStream zipOutputStream = new ZipOutputStream(fos);
        for (File file : listFiles) {
            FileInputStream fileInputStream = new FileInputStream(file);
            ZipEntry zipEntry = new ZipEntry(file.getName());
            zipOutputStream.putNextEntry(zipEntry);
            final byte[] bytes = new byte[1024];
            int len;
            while ((len = fileInputStream.read(bytes)) >= 0) {
                zipOutputStream.write(bytes, 0, len);
            }
            fileInputStream.close();
        }
        zipOutputStream.close();
        fos.close();
        logger.info(" <<< FileUtils.zipFiles <<< ");
        return true;
    }

    public static File[] listFolder(File file) {
        return file.listFiles(new FilenameFilter() {
            @Override
            public boolean accept(File dir, String name) {
                return new File(dir, name).isDirectory();
            }
        });
    }

}
