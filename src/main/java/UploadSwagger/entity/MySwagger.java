package UploadSwagger.entity;

import UploadSwagger.constant.CommonConstant;
import io.swagger.models.Operation;
import io.swagger.models.Swagger;
import io.swagger.models.Tag;

public class MySwagger {

    private Swagger swagger;
    private Operation operation;
    private String pathKey;
    private String method;
    private Tag tag;
    public Swagger getSwagger() {
        return swagger;
    }
    public void setSwagger(Swagger swagger) {
        this.swagger = swagger;
    }
    public Operation getOperation() {
        return operation;
    }
    public void setOperation(Operation operation) {
        this.operation = operation;
    }
    public String getPathKey() {
        return pathKey;
    }
    public void setPathKey(String pathKey) {
        this.pathKey = pathKey;
    }
    public String getMethod() {
        return method;
    }
    public void setMethod(String method) {
        this.method = method;
    }
    public Tag getTag() {
        if (tag == null)
        {
            tag = new Tag();
            tag.setName(CommonConstant.TAG_UNDEFINED);
        }
        return tag;
    }
    public void setTag(Tag tag) {
        this.tag = tag;
    }

}
