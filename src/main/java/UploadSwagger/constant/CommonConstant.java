package UploadSwagger.constant;

public class CommonConstant {
    public static final String ROOT_FOLDER              =   "src/main/resources/upload-dir/";
    public static final String UPLOAD_FOLDER            =   "src/main/resources/upload-dir/inbound/";
    public static final String ZIP_NAME                 =   "zip";
    public static final String ZIP_FOLDER               =   ROOT_FOLDER + ZIP_NAME +"/";
    public static final String TAG_UNDEFINED            =   "UNDEFINED";
    public static final String POST_METHOD				=	"POST";
    public static final String GET_METHOD				=	"GET";
    public static final String PUT_METHOD				=	"PUT";
    public static final String DELETE_METHOD			=	"DELETE";
    public static final String HTTP_CODE_200			=	"200";
    public static final String JSON_FILE_EXT			=	".json";

    public static final String ZIP_FORMAT               =   ".zip";
    public static final String ALL_TAGS_ZIP_NAME        =   "AllTags";
}
